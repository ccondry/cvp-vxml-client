// generate random session ID
function getHex() {
  var possible = "0123456789ABCDEF"
  return possible.charAt(Math.floor(Math.random() * possible.length))
}

function makeCallId() {
  let id = ''
  for (let i = 0; i < 4; i++) {
    if (i !== 0) id += '-'
    for (let j = 0; j < 8; j++) {
      id += getHex()
    }
  }
  return id
}

module.exports = {
  makeCallId
}
