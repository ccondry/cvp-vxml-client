const axios = require('axios')
const xml2js = require('xml2js-es6-promise')
const util = require('util')
const utils = require('./utils')

function getFromExtVxml(json) {
  // extract the VXML return variables
  let fromExtVxml = json.vxml.form[0].block[0].var
  // console.log('fromExtVxml', fromExtVxml)
  // make em pretty
  let ret = {}
  try {
    for (const v of fromExtVxml) {
      let value = v.$.expr.replace(/\'/g,'')
      ret[v.$.name] = value
    }
  } catch (e) {
    // continue
  }
  return ret
}

async function parseVxml(data) {
  const opts = {
    explicitArray: true,
    trim: true
  }
  const js = await xml2js(data, opts)

  return js
}

function parsePrompts(prompts) {
  let ret = []
  try {
    for (const prompt of prompts) {
      let bargeIn = prompt.$.bargein === 'true'
      for (const audio of prompt.audio) {
        let text = audio._
        let wav = audio.$.src
        ret.push({wav, text, bargeIn})
      }
    }
  } catch (e) {
    // do nothing
  }
  return ret
}

function parseForms(forms) {
  let ret = []
  try {
    for (const form of forms) {
      let objects = form.object
      let blocks = form.block
      let fields = form.field
      ret.push({objects, blocks, fields})
    }
  } catch (e) {
    // do nothing
  }
  return ret
}

function parseVars(vars) {
  let ret = {}
  try {
    for (const v of vars) {
      ret[v.$.name] = v.$.expr.replace(/\'/g,'')
    }
  } catch (e) {
    // do nothing
  }
  return ret
}

// function parseParams(params) {
//   let ret = []
//   try {
//     for (const param of params) {
//       let objects = form.object
//       let blocks = form.block
//       let fields = form.field
//       ret.push({objects, blocks, fields})
//     }
//   } catch (e) {
//     // do nothing
//   }
//   return ret
// }

async function startApp({url, params = {}, app, ani, dnis}) {
  // add required parameters to whatever other params the user is providing
  params['application'] = app
  params['session.connection.local.uri'] = dnis
  params['session.connection.remote.uri'] = ani

  // generate a random call ID and add to the params
  params['callid'] = utils.makeCallId()

  // call VXML REST
  const response = await axios.get(url, {params})

  // parse VXML to JSON, get prompts out
  let js = await parseVxml(response.data)
  // console.log('prompts = ', prompts)

  // console.log('js =', js)
  // parse VXML vars
  let vars = parseVars(js.vxml.var)

  // console.log(util.inspect(js.vxml, false, null))

  // parse form
  let forms = parseForms(js.vxml.form)

  // parse VXML prompts
  let prompts = []
  try {
    prompts = parsePrompts(forms[0].objects[0].prompt)
  } catch (e) {
    // continue
  }

  // console.log(util.inspect(forms, false, null))
  // console.log('prompts = ', prettyPrompts)

  // save the cookie from response
  let cookie = response.headers['set-cookie']

  // return prompts and cookie
  return {
    vars,
    prompts,
    cookie
  }
}

async function testContextService({
  host,
  port = '7000',
  app,
  dnis,
  ani,
  params = {}
}) {
  // make CVP server base URL
  const url = `http://${host}:${port}/CVP/Server`

  // start CVP app call session
  const result = await startApp({url, params, app, ani, dnis})

  // return result
  // set up cookie in header
  const headers = {
    'Cookie': result.cookie
  }

  // get next page from CVP
  const page2 = await axios.get(url, {headers})

  // get next page from CVP
  const page3 = await axios.get(url, {headers})

  // xml parsing options
  const opts = {
    explicitArray: true,
    trim: true
  }

  // parse xml response to json
  let js
  try {
    js = await xml2js(page3.data, opts)
    // console.log(util.inspect(js, false, null))
  } catch(e) {
    console.log('error parsing VXML to JSON:', e)
    throw e
  }

  // return results
  try {
    return getFromExtVxml(js)
  } catch (e) {
    console.log('error parsing VXML return params: ', e)
    throw e
  }
}

module.exports = {
  testContextService,
  startApp
}
