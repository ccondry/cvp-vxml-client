const cvp = require('../src/index')

const ani = '5551112222'
cvp.testContextService({
  host: 'cvp1.dcloud.cisco.com',
  port: '7000',
  app: 'Get_Customer',
  dnis: '7700',
  ani,
  params: {
    'q': ani,
    'field': 'query_string'
  }
})
.then(fromExtVxml => {
  // console.log(util.inspect(fromExtVxml, false, null))
  if (fromExtVxml.caller_input) {
    // success
    console.log('CS lookup success')
    console.log('CVP returned', fromExtVxml)
  } else {
    // failed
    console.log('CS lookup failed')
  }

})
.catch(error => {
  console.log(error)
})
